from django.contrib import admin

# Register your models here.
from .models import Student, Teacher, Paper, Question, Grade



admin.site.site_header = '在线考试系统后台'
admin.site.site_title = '在线考试系统'

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('id','name','sex','dept','major','email')
    list_display_links = ('id','name')  #点击哪些信息可以进入编辑页面
    search_fields = ['name','dept','major','birth']
    list_filter = ['name', 'dept', 'major', 'birth']


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'sex', 'dept', 'password', 'email', 'birth')
    list_display_links = ('id', 'name')
    search_fields = ['name', 'dept', 'birth']
    list_filter = ['name', 'dept']


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id','subject','title','optionA','optionB','optionC','optionD','answer','level','score')


@admin.register(Paper)
class PaperAdmin(admin.ModelAdmin):
    list_display = ('tid','subject','major','examtime')
    search_fields = ['tid', 'subject']

@admin.register(Grade)
class GradeAdmin(admin.ModelAdmin):
    list_display = ('sid','subject','grade')
    search_fields = ['subject']

