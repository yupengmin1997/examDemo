from django.shortcuts import render, redirect

# Create your views here.
from student.models import Student, Paper, Grade, Teacher, Question


def index(request):
    return render(request, 'index.html')


def toIndex(request):
    return render(request, 'index.html')


def studentLogin(request):
    if request.method == 'POST':
        stuId = request.POST.get('id', '')
        pwd = request.POST.get('password', '')
        student = Student.objects.get(id=stuId)
        if pwd == student.password:
            # 查询考试信息
            paper = Paper.objects.filter(major=student.major)
            # 查询成绩信息
            grade = Grade.objects.filter(sid=student.id)
            return render(request, 'index.html', {'student': student, 'paper': paper, 'grade': grade})
        else:
            return render(request, 'index.html', {'message': '密码不正确'})


def teacherLogin(request):
    if request.method == 'POST':
        teaId = request.POST.get('id', '')
        pwd = request.POST.get('password', '')
        teacher = Teacher.objects.get(id=teaId)

        if pwd == teacher.password:
            # 在试卷表 paper 找到该老师发布的试题
            paper = Paper.objects.filter(tid=teacher.id)

            data1 = Grade.objects.filter(subject='数据库原理', grade__lt=60).count()
            data2 = Grade.objects.filter(subject='数据库原理', grade__gte=60, grade__lt=70).count()
            data3 = Grade.objects.filter(subject='数据库原理', grade__gte=70, grade__lt=80).count()
            data4 = Grade.objects.filter(subject='数据库原理', grade__gte=80, grade__lt=90).count()
            data5 = Grade.objects.filter(subject='数据库原理', grade__gte=90).count()

            data_1 = {'data1': data1, 'data2': data2, 'data3': data3, 'data4': data4, 'data5': data5}
            return render(request, 'teacher.html', {'teacher': teacher, 'paper': paper, 'data_1': data_1})
        else:
            return render(request, 'index.html', {'message': '密码不正确'})


# 教师退出
def logOut(request):
    return redirect('/toIndex/')


# 开始考试
def startExam(request):
    sid = request.GET.get('sid', '')
    sub = request.GET.get('subject')

    student = Student.objects.get(id=sid)
    paper = Paper.objects.filter(subject=sub)

    return render(request, 'exam.html', {'student': student, 'paper': paper, 'subject': sub})


# 计算成绩
def calGrade(request):
    # 得到学号和科目
    if request.method == 'POST':
        sid = request.POST.get('sid', '')
        subject1 = request.POST.get('subject', '')

        # 生成学生、试卷、成绩实例
        student = Student.objects.get(id=sid)
        paper = Paper.objects.filter(major=student.major)
        grade = Grade.objects.filter(sid=student.id)

        # 计算试卷成绩
        question = Paper.objects.filter(subject=subject1).values('pid__id', 'pid__answer', 'pid__score')

        mygrade = 0
        for p in question:
            qid = str(p['pid__id'])  # 通过pid找到题号
            myans = request.POST.get(qid)  # 得到学生关于该题的作答
            ans = p['pid__answer']  # 得到正确答案
            if myans == ans:
                mygrade += p['pid__score']  # 若一致,得到该题的分数,累加mygrade变量

        # 将成绩插入成绩表中
        Grade.objects.create(sid_id=sid, subject=subject1, grade=mygrade)
        # 将成绩渲染到模板
        return render(request, 'index.html', {'student': student, 'paper': paper, 'grade': grade})


def showGrade(request):
    subject1 = request.GET.get('subject')
    grade = Grade.objects.filter(subject=subject1)

    data1 = Grade.objects.filter(subject=subject1, grade__lt=60).count()
    data2 = Grade.objects.filter(subject=subject1, grade__gte=60, grade__lt=70).count()
    data3 = Grade.objects.filter(subject=subject1, grade__gte=70, grade__lt=80).count()
    data4 = Grade.objects.filter(subject=subject1, grade__gte=80, grade__lt=90).count()
    data5 = Grade.objects.filter(subject=subject1, grade__gte=90).count()

    data = {'data1': data1, 'data2': data2, 'data3': data3, 'data4': data4, 'data5': data5}

    return render(request, 'showGrade.html', {'grade': grade, 'data': data, 'subject': subject1})
